def test_add(): #patron de lineas de test:
    from addition import add #arrange (import fxn to test)
    sum_result = add(4, 2) #act (uso la fx)
    assert sum_result == 6 #assert (comparo el resultado con el esperado)

# TDD: Test-Driven Development 
# Red-Green-Refactor cycle